��            )   �      �     �     �     �     �     �     �     �     �            
   "  	   -  (   7  M   `  1   �     �  +   �       "        A  ,   X  f   �  \   �     I  W   M     �  .   �     �     �  �         #     '   5  &   ]  
   �  
   �     �     �  $   �     �     �     	  j   &	  �   �	  l   #
     �
  f   �
       2   $  M   W  O   �  �   �  �   �     J  �   Q       m   0  )   �  )   �                             
                                                     	                                                        OK  (Terminal command) Apps/Settings COMMAND could not be found Exit Files Finder Internet: ok It will be replaced by: Language New search No region No region (results are shown in English) No results found for files/folders with names containing this expression, in  No results found for this application or setting: Nothing to do Press any key to close this terminal window Run Search parameter is empty, exiting Searching, please wait The current Region code for web searches is: There are instalable packages that match that query, available in the Repository (and their version):  WARNING: Closing this terminal window will close also the $query instance running inside it! Web You need to have yad installed. Please make sure it is installed and re-run this script ddgr not installed! is a command meant to be run from the terminal not empty, continuing starting Finder Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-12-23 14:26+0100
Last-Translator: Wallon
Language-Team: Russian (https://app.transifex.com/anticapitalista/teams/10162/ru/)
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
X-Generator: Poedit 3.2.2
 Хорошо (Команда терминала) Приложения/настройки Нельзя найти КОМАНДУ Выход Файлы Поисковик Интернет: хорошо Он будет заменён на: Язык Новый поиск Нет региона Нет региона (результаты показываются на английском языке) Не найдено результатов для файлов/папок с именами, содержащими это выражение, в Для этого приложения или настройки результатов не найдено: Нечего делать Нажмите любую клавишу, чтобы закрыть это окно терминала Запустить Параметр поиска пуст, выход Производится поиск, пожалуйста, подождите Текущий код региона для поиска в Интернете: В репозитории доступны пакеты для установки, соответствующие этому запросу (и их версии): ВНИМАНИЕ: Закрытие этого окна терминала приведёт к закрытию и экземпляра $query, запущенного в нём! Веб Необходимо, чтобы у вас был установлен yad. Убедитесь, что он установлен, и повторно запустите этот скрипт ddgr не установлен! — это команда, предназначенная для выполнения из терминала не пусто, продолжается запускается Поисковик 